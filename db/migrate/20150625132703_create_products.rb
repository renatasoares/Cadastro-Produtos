class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :code
      t.string :description
      t.string :department
      t.string :brand
      t.string :aliquot
      t.string :price

      t.timestamps null: false
    end
  end
end
