json.array!(@products) do |product|
  json.extract! product, :id, :code, :description, :department, :brand, :aliquot, :price
  json.url product_url(product, format: :json)
end
